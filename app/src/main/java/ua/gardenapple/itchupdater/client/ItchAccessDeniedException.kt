package ua.gardenapple.itchupdater.client

class ItchAccessDeniedException(message: String) : Exception(message)